import markdown
import sentry_sdk
from django.db import models
from sentry_sdk import add_breadcrumb

from quizbot.models import State, Trigger, Message

START_STATE_NAME = 'start'


def report_through_sentry(label, issue, data):
    # msg_template = "{issue}- data: {data}"
    # msg = msg_template.format(issue=issue, data=data)
    sentry_sdk.capture_message(issue, label)


class StateManager(models.Model):
    lang_dict = {
        "English": 'en',
        "Spanish (Español)": 'es',
        "Chinese - Simplified (简体中文)": 'zh-hans',
        "Chinese - Traditional (繁體中文)": 'zh-hant'
    }

    @staticmethod
    def add_language_setting_to_context(data):
        """ Adds lang (language setting) when called from a state
            Only supposed to be used after the initial_state loads
        """

        user_text = data.get('user_text')
        lang_choice = StateManager.lang_dict.get(user_text, 'en')

        data['context']['lang'] = lang_choice

        return data

    @staticmethod
    def create_initial_user_type_dict(triggers):
        """ Builds a dictionary of user_type roles from triggers in the target state """
        user_type_dict_English = {}
        user_type_dict_English_keys = []
        for trigger in triggers:
            if trigger.lang == 'en':
                user_type_dict_English[trigger.intent_text] = [trigger.intent_text]
                user_type_dict_English_keys.append(trigger.intent_text)
        return user_type_dict_English, user_type_dict_English_keys

    @staticmethod
    def update_user_type_dict(triggers, user_type_dict_English, user_type_dict_English_keys, lang_list):
        """ Takes an dict of user_types in English and adds corresponding entries for other languages """
        for lang in lang_list:
            tr_sublist = []
            for trigger in triggers:
                if trigger.lang == lang:
                    tr_sublist.append(trigger)
            for index in range(3):
                user_type_dict_English[user_type_dict_English_keys[index]].append(tr_sublist[index].intent_text)
        return user_type_dict_English

    @staticmethod
    def generate_user_type_dict():
        """ Builds a dictionary of user_types for the 'is-this-your-first-yes' state
        Used to set context dictionary's user_type
        """
        state = State.objects.filter(state_name='is-this-your-first-yes')[0]
        triggers = Trigger.objects.filter(from_state=state, is_button=False)

        user_type_dict_English, user_type_dict_English_keys = StateManager.create_initial_user_type_dict(triggers)

        # lang_dict = self.send_language_dictionary(self)
        lang_list = StateManager.lang_dict.values()

        user_type_dict = StateManager.update_user_type_dict(
            triggers,
            user_type_dict_English,
            user_type_dict_English_keys,
            lang_list
        )
        return user_type_dict

    @staticmethod
    def add_user_type_to_context(data):
        """ Adds the user_type when called from a state
        'WSNYC Volunteer + Educator'
        'WSNYC Student'
        """

        user_type_dict = StateManager.generate_user_type_dict()

        user_type = ''
        for val in user_type_dict.values():
            if data['user_text'] in val:
                user_type = list(user_type_dict.keys())[list(user_type_dict.values()).index(val)]

        data['context']['user_type'] = user_type
        return data

    @staticmethod
    def state_serializer(state):
        return {'state_name': state}

    @staticmethod
    def message_serializer(state, lang):
        messages = Message.objects.filter(state=state, lang=lang).values_list("sequence_number", "bot_text")
        message_serializer_list = []

        for message in messages:
            message_serializer_list.append(
                {
                    'sequence_number': message[0],
                    'bot_text': markdown.markdown(message[1], extensions=['attr_list'])
                }
            )
        return message_serializer_list

    @staticmethod
    def trigger_serializer(state, lang, is_button=False):
        triggers = Trigger.objects.filter(from_state=state, lang=lang, is_button=is_button) \
            .values_list("intent_text", "to_state")
        trigger_serializer_list = []
        for trigger in triggers:
            state_result = State.objects.get(pk=trigger[1])
            trigger_serializer_list.append(
                {
                    'intent_text': trigger[0],
                    'to_state': state_result.state_name
                }
            )
        return trigger_serializer_list

    @staticmethod
    def get_current_state_from_database(data, checkpoint):
        """returns current state as a JSON object"""
        current_state = State.objects.get(state_name=data['state_name'], checkpoint=checkpoint)

        # creates return values
        state_serializer = StateManager.state_serializer(current_state.state_name)
        message_serializer = StateManager.message_serializer(current_state, data['context']['lang'])
        trigger_serializer = StateManager.trigger_serializer(current_state, data['context']['lang'])

        return {
            "state": state_serializer,
            "messages": message_serializer,
            "triggers": trigger_serializer,
            "context": {'lang': 'en'},
        }

    @staticmethod
    def has_next_trigger(state_name, checkpoint):
        initial_state = State.objects.get(state_name=state_name, checkpoint=checkpoint)
        triggers = Trigger.objects.filter(from_state=initial_state, is_button=False)
        return any(True if trigger.intent_text == "__next__" else False for trigger in triggers)

    @staticmethod
    def get_next_state(data, checkpoint):

        try:
            # Test if the state exists in the database
            initial_state = State.objects.get(state_name=data['state_name'], checkpoint=checkpoint)
        except State.DoesNotExist as err:
            sentry_sdk.capture_exception(err)
            content = {'error_message': 'No matching state'}
            return content

        triggers = Trigger.objects.filter(from_state=initial_state, is_button=False)

        try:
            # If the user text is not found in triggers, sets the value as __default__
            intent_text = next(
                t.intent_text for t in Trigger.objects.filter(from_state=initial_state, is_button=False) if
                t.intent_text.lower() == data['user_text'].lower())
        except StopIteration:
            data['user_text'] = "__default__"
        else:
            data['user_text'] = intent_text

        for trigger in triggers:
            # finds the next state
            if trigger.intent_text.lower() == data['user_text'].lower():
                next_state = trigger.to_state
                break

        # Test for and execute functions associated with a state
        state_test = State.objects.filter(state_name=data['state_name'], checkpoint=checkpoint)
        trig = Trigger.objects.filter(from_state=state_test[0], intent_text=data['user_text'], is_button=False)

        if trig[0].update_kwargs:
            for func_name in trig[0].update_kwargs['context_functions']:
                output = getattr(StateManager, func_name, lambda: 'Not found')(data)
                data['context'] = output['context']

        # Build the response with the data for the next state
        state_serializer = StateManager.state_serializer(next_state.state_name)
        # message_serializer.extend(StateManager.message_serializer(next_state, data['context']['lang']))
        message_serializer = StateManager.message_serializer(next_state, data['context']['lang'])
        trigger_serializer = StateManager.trigger_serializer(next_state, data['context']['lang'], is_button=True)
        response = {
            "state": state_serializer,
            "messages": message_serializer,
            "triggers": trigger_serializer,
            "context": data['context'],
            # "has_next": StateManager.has_next_trigger(next_state.state_name, checkpoint),
        }
        return response

    @staticmethod
    def manage_state(data, checkpoint):
        """ input the current state (state_name) + message user selected (user_text)
        get_next_state_from_database returns a JSON object of the next state's data necessary to fill the frontend.
        """
        add_breadcrumb(
            category='Database Call Function',
            message='get_next_state_from_database',
            level='info'
        )

        if "state_name" not in data or data['state_name'] == '':
            # Set the state_name if value missing
            report_through_sentry('info', 'Missing state_name', data)

            data['state_name'] = 'select-language'
            data['user_text'] = data.get("user_text", '')

        if "user_text" not in data or data['user_text'] == '':
            # Set the user_text if value missing
            report_through_sentry('info', 'Missing user_text', data)

            data['user_text'] = ''

        if not data.get('context') or not data.get('context').get('lang'):
            # Set the context if value missing
            data['context'] = {'lang': 'en'}

        initial_state = State.objects.get(state_name=data['state_name'], checkpoint=checkpoint)

        if StateManager.has_next_trigger(data['state_name'], checkpoint):
            # If the first state has __next__ trigger, adds the current state message to the response
            data['user_text'] = '__next__'
            message_serializer = StateManager.message_serializer(initial_state, data['context']['lang'])
        else:
            message_serializer = []

        response = StateManager.get_next_state(data, checkpoint)
        response["messages"] = message_serializer + response["messages"]

        while StateManager.has_next_trigger(response["state"]["state_name"], checkpoint):
            # While the response state has __next__ trigger, auto switches to the next state
            data["state_name"] = response["state"]["state_name"]
            data["user_text"] = "__next__"
            data["context"] = response["context"]

            current_state = State.objects.get(state_name=data['state_name'], checkpoint=checkpoint)
            message_serializer += StateManager.message_serializer(current_state, data['context']['lang'])

            response = StateManager.get_next_state(data, checkpoint)
            response["messages"] = message_serializer + response["messages"]

        return response
