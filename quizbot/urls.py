from django.urls import path

from quizbot import views

urlpatterns = [
    # sequence is important!
    path('', views.quiz, name='quiz'),
    path('api/', views.APINextStateReply.as_view(), name='quizbot-next-state'),
    # path('bot/<bot_name>/', views.conversation, name='conversation'),
    path('documents/', views.DocumentListView.as_view(), name='quizbot-document-list'),
    path('documents/create-flow/', views.create_flow, name='quizbot-create-flow'),
    path('upload/', views.upload, name='quizbot-upload'),
    path('<str:room_name>/', views.room, name='quizbot-room'),
]
