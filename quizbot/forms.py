from django.forms import ModelForm

from quizbot.models import Document


class DocumentForm(ModelForm):
    class Meta:
        model = Document
        fields = ['file']
