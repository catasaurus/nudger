from django.contrib import admin

from .models import (
    State,
    Message,
    Trigger,
    InteractionData,
    Checkpoint,
    Document,
    Convo,
    ConversationTracker
)

# Register your models here.
admin.site.register(State)
admin.site.register(Message)
admin.site.register(Trigger)
admin.site.register(Checkpoint)
admin.site.register(Document)
admin.site.register(Convo)
admin.site.register(ConversationTracker)


class InteractionDataAdmin(admin.ModelAdmin):
    readonly_fields = ('send_time',)


admin.site.register(InteractionData, InteractionDataAdmin)
