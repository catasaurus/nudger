import io
from collections import abc
from pathlib import Path

import numpy as np
import pandas as pd
import yaml


def coerce_dialog_tree_series(states: (str, dict, Path, io.TextIOWrapper, pd.Series, list, np.ndarray, pd.Series)):
    """ Ensure that (yamlfilepath | list) -> dict -> Series """
    if isinstance(states, pd.Series):
        return states
    if isinstance(states, (str, Path)):
        with Path(states).open() as fin:
            states = yaml.full_load(fin)
    if isinstance(states, io.TextIOWrapper):
        states = yaml.full_load(states)
    if isinstance(states, abc.Mapping):
        return pd.Series(states)
    if isinstance(states, (list, np.ndarray)):
        states = pd.Series(
            states,
            index=[s.get('name', str(i)) for (i, s) in enumerate(states)])
        states.index.name = 'name'
        return states
    raise ValueError(f"Unable to coerce {type(states)} into pd.Series:\n  states={str(states)[:130]}...")
