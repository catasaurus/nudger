""" Intent matching functions that return true or false for the intent matching the statement

(dependency injections for intent recognitizer check_match)
"""
from .classifiers import classify_intent
from qary.spacy_language_model import nlp
import numpy as np


def exact(statement, intent):
    return statement == intent


def fuzzy(statement, intent):
    """ Use kalika's emotional word/intent recognizer """
    return classify_intent(statement) == intent


def keyword(statement, intent):
    """ Use kalika's emotional word/intent recognizer """
    # TODO: tokenize statement with memoized tokenizer
    return intent in statement


def default(statement, intent):
    return exact(statement, intent)


def spacy(statement, intent, threshold=0.75):
    """ 
    
    >>> spacy("Yes", "yup", threshold=0.75)
    True
    >>> spacy("Yes", "nope")
    False
    """
    v_statement = nlp(statement).vector
    v_intent = nlp(intent).vector
    v_statement /= np.linalg.norm(v_statement)
    v_intent /= np.linalg.norm(v_intent)
    # np.linalg.norm(v_intent - v_statement)
    cos_similarity = v_intent.dot(v_statement)
    return cos_similarity > threshold


