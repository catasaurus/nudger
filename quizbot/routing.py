from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(r"ws/quiz/(?P<room_name>[A-z0-9]+)/$", consumers.QuizRoomConsumer.as_asgi()),
]
