# Generated by Django 4.1.3 on 2023-04-03 15:36

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Class',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, help_text='Name of a class at the university', max_length=128, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(default='', help_text='The subject of the nudge (128 characters or less)', max_length=128)),
                ('body', models.TextField(default='', help_text='The message to send to students.')),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(help_text='Used in addressing the student in messages.', max_length=128)),
                ('last_name', models.CharField(help_text='Used to identify student in forms', max_length=128)),
                ('email', models.EmailField(default='', help_text='Used to send nudges to students when selected as the delivery method', max_length=128)),
                ('mobile_number', models.CharField(help_text='Used to send nudges to students when selected as the delivery method', max_length=32)),
                ('txt_ok_ind', models.BooleanField(default=True, help_text='Indicates the student accepted receiving texted nudges')),
                ('email_ok_ind', models.BooleanField(default=True, help_text='Indicates the student accepted receiving email nudges')),
                ('dim_pgm_of_study_cur', models.CharField(default='', help_text='?????????', max_length=128)),
                ('birth_date', models.DateField(default='', help_text='Birthday in YYYY-MM-DD')),
                ('overall_credits', models.FloatField(default=0.0, help_text="Shows the student's total number of course credits earned so far")),
                ('overall_gpa', models.FloatField(default=0.0, help_text="Shows the student's cumulative GPA")),
                ('canvas_user_id', models.ForeignKey(default='', help_text="The student's id in the Canvas LMS", on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('content', models.TextField()),
                ('date_posted', models.DateTimeField(default=django.utils.timezone.now)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Nudge',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('scheduled_datetime', models.DateTimeField(default='', help_text='The date/time selected to send the nudge. If left empty, the nudge will send immediately after saving.')),
                ('sent_datetime', models.DateTimeField(blank=True, help_text='When the nudge was sent', null=True)),
                ('nudge_sender', models.CharField(default='', help_text='The instructor who scheduled the nudge (128 characters or less).', max_length=128)),
                ('nudge_type', models.CharField(choices=[('immediate_email', 'Immediate Email'), ('scheduled_email', 'Scheduled Email'), ('immediate_sms', 'Immediate SMS'), ('scheduled_sms', 'Scheduled SMS')], default='immediate_email', help_text='Immediate emails/SMS will be sent upon submiting this form.  Scheduled emails or SMSs will be sent out at a time you schedule.', max_length=64)),
                ('message', models.ForeignKey(default=0, help_text='Each message contains a subject and body.  Only the subject is currently displayed.', on_delete=django.db.models.deletion.RESTRICT, to='sms_nudger.message')),
                ('student', models.ManyToManyField(help_text='The students who will receive the message', to='sms_nudger.student')),
            ],
        ),
    ]
