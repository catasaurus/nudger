from django.contrib.auth.hashers import make_password
from users.models import Profile
from sms_nudger.models import Student, Nudge, Message
from faqbot.models import Faq
from django.contrib.auth.models import User
import os
import sys
import django
import json
from pathlib import Path

# Set project directory
base_dir = Path(__file__).resolve().parent.parent
sys.path.append(os.path.abspath(base_dir))

# Set path to project settings.py
os.environ["DJANGO_SETTINGS_MODULE"] = "nudger.settings"
django.setup()


# Import database models (Must happen after settings above)

# Import password creation tools


def create_test_users():
    with open("data/sample_users.json") as f:
        test_users = json.load(f)

    for user in test_users:
        user = User(
            username=user["username"],
            password=make_password(user["password"]),
            is_staff=user["is_staff"],
            is_active=user["is_active"],
            is_superuser=user["is_superuser"],
        )
        user.save()


def create_test_students():
    with open("data/sample_students.json") as f:
        test_students = json.load(f)

    for student in test_students:
        student = Student(
            first_name=student["first_name"],
            last_name=student["last_name"],
            email=student["email"],
            mobile_number=student["mobile_number"],
            txt_ok_ind=student["txt_ok_ind"],
            email_ok_ind=student["email_ok_ind"],
            dim_pgm_of_study_cur=student["dim_pgm_of_study_cur"],
            birth_date=student["birth_date"],
            overall_credits=student["overall_credits"],
            overall_gpa=student["overall_gpa"],
            canvas_user_id=User(student["canvas_user_id"]),
        )
        student.save()


def create_test_messages():
    with open("data/sample_messages.json") as f:
        test_messages = json.load(f)

    for message in test_messages:
        message = Message(subject=message["subject"], body=message["body"])
        message.save()


def create_test_nudges():
    with open("data/sample_nudges.json") as f:
        test_nudges = json.load(f)

    for nudge in test_nudges:
        nudge = Nudge(
            scheduled_datetime=nudge["scheduled_datetime"],
            sent_datetime=nudge["sent_datetime"],
            student=Student(nudge["student"]),
            nudge_sender=nudge["nudge_sender"],
            nudge_type=nudge["nudge_type"],
            message=Message(nudge["message"]),
        )
        nudge.save()


def create_test_faqs():
    with open("data/sample_faq.json") as f:
        test_messages = json.load(f)

    for message in test_messages:
        message = Faq(
            created_on=message["created_on"],
            user_id=User.objects.get(id=message["user_id"]),
            question=message["question"],
            answer=message["answer"]
        )
        message.save()


def create_test_data():
    create_test_users()
    create_test_students()
    create_test_messages()
    create_test_nudges()
    create_test_faqs()


if __name__ == "__main__":
    create_test_data()
