from django.db import models
from django.db.models.signals import post_save, pre_save, m2m_changed

from django.utils import timezone

from django.urls import reverse

from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

from sms_nudger.tasks import email_nudge, sms_nudge, crontab_email_nudge, crontab_sms_nudge


class Class(models.Model):
    name = models.CharField(null=True, blank=True, max_length=128, help_text="Name of a class at the university")


class Student(models.Model):
    """ Contains data downloaded from Canvas to RDS and periodically updated by cron job """
    first_name = models.CharField(null=False, max_length=128,
                                  help_text="Used in addressing the student in messages."
                                  )
    last_name = models.CharField(null=False, max_length=128, help_text="Used to identify student in forms")
    email = models.EmailField(null=False, max_length=128, default='', help_text="Used to send nudges to students when selected as the delivery method")
    mobile_number = models.CharField(max_length=32, help_text="Used to send nudges to students when selected as the delivery method")
    txt_ok_ind = models.BooleanField(null=False, default=True, help_text="Indicates the student accepted receiving texted nudges")
    email_ok_ind = models.BooleanField(null=False, default=True, help_text="Indicates the student accepted receiving email nudges")
    dim_pgm_of_study_cur = models.CharField(null=False, max_length=128, default='', help_text="?????????")
    birth_date = models.DateField(default='', help_text="Birthday in YYYY-MM-DD")
    overall_credits = models.FloatField(null=False, default=0.0, help_text="Shows the student's total number of course credits earned so far")
    overall_gpa = models.FloatField(null=False, default=0.0, help_text="Shows the student's cumulative GPA")
    canvas_user_id = models.ForeignKey(User, null=False, on_delete=models.CASCADE, default='', help_text="The student's id in the Canvas LMS")

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

# Nudge columns: scheduled_datetime, sent_datetime, student (ForeignKey(Student)), nudge_message, nudge_subject, nudge_sender, nudge_type (CharField(max_len=64, choices=['immediate_email', 'scheduled_email', 'immediate_sms', 'scheduled_sms']))'
# nudge_message, nudge_subject, nudge_sender, nudge_type (CharField(max_len=64, choices=['immediate_email', 'scheduled_email', 'immediate_sms', 'scheduled_sms']))'


class Message(models.Model):
    subject = models.CharField(null=False, max_length=128, default='', help_text="The subject of the nudge (128 characters or less)")
    body = models.TextField(null=False, default='', help_text="The message to send to students.")

    def __str__(self):
        return f'{self.subject}'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)


class Nudge(models.Model):
    scheduled_datetime = models.DateTimeField(default='', help_text="The date/time selected to send the nudge. If left empty, the nudge will send immediately after saving.")
    sent_datetime = models.DateTimeField(blank=True, null=True, help_text="When the nudge was sent")
    # student = models.ForeignKey(Student, null=False, on_delete=models.CASCADE, default='', help_text="The recipient of a nudge")
    student = models.ManyToManyField(Student, help_text="The students who will receive the message")
    nudge_sender = models.CharField(null=False, max_length=128, default='', help_text="The instructor who scheduled the nudge (128 characters or less).")
    choices = [('immediate_email', 'Immediate Email'),
               ('scheduled_email', 'Scheduled Email'),
               ('immediate_sms', 'Immediate SMS'),
               ('scheduled_sms', 'Scheduled SMS')]
    nudge_type = models.CharField(max_length=64, choices=choices, default='immediate_email', help_text="Immediate emails/SMS will be sent upon submiting this form.  Scheduled emails or SMSs will be sent out at a time you schedule.")
    message = models.ForeignKey(Message, null=False, default=0, on_delete=models.RESTRICT, help_text="Each message contains a subject and body.  Only the subject is currently displayed.")

    def __str__(self):
        return f'{self.message} - {self.scheduled_datetime}'

    # Collected the nudge data
    def save(self, *args, **kwargs):
        # Saves the message as a Nudge Record

        super().save(*args, **kwargs)

# Sends the Nudge instance data to the right function for scheduling
# This should not be part of the Nudge class
def nudge_scheduler(sender, instance, action, **kwargs):

    if action == 'post_add':
        for student in instance.student.all():
            if student.email_ok_ind == True and (instance.nudge_type == 'immediate_email' or instance.nudge_type == 'scheduled_email'):
                print(f"EMAIL TRUE: {student.first_name}")

            # Selects the function based on delivery method and time
            # Self values are for message content
            # Regular values are for student information
            # These functions also save to Celery models ("Task results" and, possibly "Periodic tasks")
                if(instance.nudge_type == 'immediate_email'):
                    email_nudge(
                        student.first_name,
                        student.email,
                        instance.message.body
                    )
                elif(instance.nudge_type == 'scheduled_email'):
                    crontab_email_nudge(
                        student.first_name,
                        student.email,
                        instance.message.body,
                        instance.scheduled_datetime
                    )
                else:
                    return "Error"

            if student.email_ok_ind == True and (instance.nudge_type == 'immediate_sms' or instance.nudge_type == 'scheduled_sms'):
                print(f"SMS TRUE: {student.first_name}")

                if(instance.nudge_type == 'immediate_sms'):
                    sms_nudge(
                        student.first_name,
                        student.phone,
                        instance.message.body
                    )
                elif(instance.nudge_type == 'scheduled_sms'):
                    crontab_sms_nudge(
                        student.first_name,
                        student.phone,
                        instance.message.body,
                        instance.scheduled_datetime
                    )
                else:
                    return "Error"

    else:
        print("NOOOOOOOOO!")


# Triggers nudge_scheduler when student field has been written to nudge
# ManyToManyFields have two steps - save and then add M2M field to instance
m2m_changed.connect(nudge_scheduler, sender=Nudge.student.through)


class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    # If a user is deleted, all their posts are deleted as well
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})
