from twilio.rest import Client

import os

from dotenv import load_dotenv, dotenv_values

# read the .env file
load_dotenv()
ENV = dotenv_values()  # WARNING: Does NOT load system (os.environ) variables such as PATH


def send_sms_nudge(name, phone, body):
    account_sid = os.environ.get("ACCOUNT_SID")
    auth_token = os.environ.get("AUTH_TOKEN")

    # TODO: Need to make sure phone numbers are cleaned
    # TODO: May need to concatonate a + in front of phone numbers
    cleaned_phone = phone

    client = Client(account_sid, auth_token)

    message = client.messages.create(
        to=f"+{cleaned_phone}",
        from_=os.environ.get("FROM_PHONE"),
        body=f"{name}, {body}",
    )
    return message
