###########
# BUILDER #
###########

# pull official base image
FROM python:3.9 as builder

# set work directory
WORKDIR /nudger

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# lint
RUN pip install --upgrade pip
RUN pip install flake8
COPY . .
# RUN flake8 --ignore=E501,F401,F541 .

# install psycopg2 dependencies
RUN apt-get update
RUN apt-get install nano python3-dev libpq-dev -y

# install dependencies
COPY ./requirements.txt .
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /nudger/wheels -r requirements.txt


#########
# FINAL #
#########

# pull official base image
FROM python:3.9

# create directory for the app user
RUN mkdir -p /home/app

# create the app user
RUN addgroup --system app && adduser --system app --ingroup app

# create the appropriate directories
ENV HOME=/home/app
ENV APP_HOME=/home/app/web
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/static
# COPY ./nudger/static $APP_HOME/static
WORKDIR $APP_HOME

# install psycopg2 dependencies
RUN apt-get update
RUN apt-get install nano python3-dev libpq-dev -y

# install dependencies
RUN pip install --upgrade pip
COPY --from=builder /nudger/wheels /wheels
COPY --from=builder /nudger/requirements.txt .
RUN pip install --no-cache /wheels/*


# chown all the files to the app user
COPY --chown=app:app . $APP_HOME
RUN chown app:app -R $APP_HOME
RUN chown -R app:app $APP_HOME

# change to the app user
USER app
