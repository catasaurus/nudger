# start-django production ASGI server (daphne)

if [ -f ".env" ] ; then
    source .env
fi

if [ -f ".venv/bin/activate" ] ; then
    source .venv/bin/activate
fi

# daphne -b 0.0.0.0 nudger.asgi:application & 
daphne -b 0.0.0.0 nudger.asgi:application
