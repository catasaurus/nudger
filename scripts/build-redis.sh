sudo apt install lsb-release
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt-get update
sudo apt-get install -y redis

# redis_enabled = $(sudo service redis status | grep -ie enabled )
# if [ "$redis_enabled" == "OK" ] ; then
# else
sudo service redis enable
sudo service redis start
# fi
