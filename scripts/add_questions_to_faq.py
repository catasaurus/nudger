import pandas as pd

from faqbot.models import MathQuestion, StudentQuestion


def create_math_questions_from_file(path):
    """
    Inserts records from a '.csv' file to the database with a given path
    path = 'data/general_student_questions_by_10s.csv'
    """
    data = pd.read_csv(path).to_dict("records")

    for item in data:
        question = MathQuestion.objects.filter(question=item["question"])
        if question:
            print(f"{question} already exists!")
        else:
            new_question = MathQuestion(question=item["question"], answer=item["answer"])
            new_question.save()
            print(f'{item["question"]} added to the database!')


def create_student_questions_from_file(path):
    """
    Inserts records from a '.csv' file to the database with a given path
    path example = 'data/general_questions_for_10s.csv'
    """
    data = pd.read_csv(path).to_dict("records")

    for item in data:
        question = StudentQuestion.objects.filter(question=item["question"])
        if question:
            print(f"{question} already exists!")
        else:
            new_question = StudentQuestion(question=item["question"], answer=item["answer"])
            new_question.save()
            print(f'{item["question"]} added to the database!')
