# import asyncio
# import time
# import pytest

# from asgiref.sync import sync_to_async
# from channels.db import database_sync_to_async
# from channels.routing import URLRouter
# from channels.testing import ChannelsLiveServerTestCase, WebsocketCommunicator
# from selenium import webdriver
# from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.wait import WebDriverWait

# from django.urls import re_path
from django.test import TestCase

from chat_async.load_yaml import create_states_and_messages, create_triggers
# from chat_async.views import NextStateReply
from chat_async.models import State
# from .consumers import ChatAsyncConsumer


class NextStateReplyTestCase(TestCase):
    """ Cases to test state data from NextStateReply in chat_async.views """
    @classmethod
    def setUpTestData(cls):
        create_states_and_messages()
        create_triggers()
        cls.assertEqual(True, True, "Test failed!")

    # TODO Tests are failing. Has to be fixed!

    # def test_blank_inputs_should_return_state(self):
    #     data = {"state_name": "", "user_text": ""}
    #     # result = NextStateReply(data)
    #     result = State.get_next_state_from_database(data)
    #     self.assertEqual(result['state']['state_name'], 'select-language')
    #
    # def test_should_return_next_state(self):
    #     data = {"state_name": "select-language", "user_text": "English"}
    #     # result = NextStateReply(data)
    #     result = State.get_next_state_from_database(data)
    #     self.assertEqual(result['state']['state_name'], 'selected-language-welcome')
    #
    # def test_should_return_error_when_blank_not_an_option(self):
    #     data = {"state_name": "selected-language-welcome", "user_text": ""}
    #     # result = NextStateReply(data)
    #     result = State.get_next_state_from_database(data)
    #     self.assertEqual(result['error_message'], 'User message is required')
    #
    # def test_should_return_error_when_input_not_viable_option(self):
    #     data = {"state_name": "selected-language-welcome", "user_text": "dklagjaljdgla"}
    #     # result = NextStateReply(data)
    #     result = State.get_next_state_from_database(data)
    #     self.assertEqual(result['error_message'], 'Response is not an option')


# @database_sync_to_async
# def setUpTestData3():
#     create_states_and_messages()
#     create_triggers()

# @pytest.mark.asyncio
# @pytest.mark.django_db(transaction=True)
# class ConsumerTest(TestCase):
#     """ Tests the ChatAsyncConsumer consumer through simulated behavior without using the browser """
#     async def test_my_consumer(self):
#         # Test database is not being set up
#         # m = await sync_to_async(setUpTestData3())()

#         application = URLRouter([
#             re_path(r'ws/chat-async/(?P<room_name>\w+)/$', ChatAsyncConsumer.as_asgi()),
#         ])

#         data = {
#             'type': 'send_welcome_message',
#             'welcome_text': 'hello world'
#         }
#         communicator = WebsocketCommunicator(application, "/ws/chat-async/testroom2/")

#         connected, subprotocol = await communicator.connect()
#         assert connected

#         pass

#         # response = await communicator.receive_from()
#         # await communicator.send_to(data)

#         # Causes test to fail without data
#         # await communicator.disconnect()


# class ChatAsyncTests(ChannelsLiveServerTestCase):
#     """ Selenium tests to run through simulate actual use in browser """
#     serve_static = True  # emulate StaticLiveServerTestCase

#     @classmethod
#     def setUpClass(cls):
#         """ Creates the test database with state data based on yaml file """
#         create_states_and_messages()
#         create_triggers()

#         super().setUpClass()
#         try:
#             # NOTE: Requires "chromedriver" binary to be installed in $PATH
#             cls.driver = webdriver.Chrome()
#         except:
#             super().tearDownClass()
#             raise

#     @classmethod
#     def tearDownClass(cls):
#         cls.driver.quit()
#         super().tearDownClass()

#     def test_enter_chatroom_and_progress_through_one_turn(self):
#         """ Enters a chatroom, loads the initial state, submits 'English', and tests for new state data """

#         self._enter_chat_room('testroom')

#         # Test that form text input field has loaded correctly
#         input_value = self.driver.find_element(by=By.CSS_SELECTOR, value="#chat-message-input").get_property('placeholder')
#         self.assertTrue('message' in input_value.lower())

#         # Test that correct buttons are being displayed
#         WebDriverWait(self.driver, 20).until(
#             lambda _: self.driver.find_element(
#                 by=By.CSS_SELECTOR, value="#button-area").is_displayed(),
#                 "The buttons never appeared."
#         )
#         button_area = self.driver.find_elements(by=By.CSS_SELECTOR, value="#button-area li label")
#         button_vals = []
#         for button in button_area:
#             button_vals.append(button.text)
#         self.assertTrue('English' in button_vals)

#         # Test that chat_log displays the welcome message in English
#         chat_log = self.driver.find_element(by=By.CSS_SELECTOR, value="#chat-log:first-child:first-child")
#         self.assertTrue('Hello' in chat_log.text)

#         # Test that chat interface can receive input
#         self._post_message('English')
#         time.sleep(20)

#         # Test that state is updated based on input
#         WebDriverWait(self.driver, 20).until(lambda _:
#             "NYC" in chat_log.text,
#             "Updated messages not recognized"
#         )

#         updated_button_area = self.driver.find_elements(by=By.CSS_SELECTOR, value="#button-area li label")
#         updated_button_vals = []
#         for updated_button in updated_button_area:
#             updated_button_vals.append(updated_button.text)
#         self.assertTrue('Ready' in updated_button_vals)

#         self._close_all_new_windows()

#     # === Utility ===

#     def _enter_chat_room(self, room_name):
#         self.driver.get(self.live_server_url + '/chat-async/')
#         ActionChains(self.driver).send_keys(room_name + '\n').perform()
#         WebDriverWait(self.driver, 2).until(
#             lambda _: room_name in self.driver.current_url
#         )

#     # Not used
#     # def _open_new_window(self):
#     #     self.driver.execute_script('window.open("about:blank", "_blank");')
#     #     self._switch_to_window(-1)

#     def _close_all_new_windows(self):
#         while len(self.driver.window_handles) > 1:
#             self._switch_to_window(-1)
#             self.driver.execute_script('window.close();')
#         if len(self.driver.window_handles) == 1:
#             self._switch_to_window(0)

#     def _switch_to_window(self, window_index):
#         self.driver.switch_to.window(self.driver.window_handles[window_index])

#     def _post_message(self, message):
#         ActionChains(self.driver).send_keys(message + '\n').perform()

#     # Not used
#     # @property
#     # def _chat_log_value(self):
#     #     return self.driver.find_element(by=By.CSS_SELECTOR, value="#chat-log").get_property('value')
