from django.apps import AppConfig


class ChatAsyncConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chat_async'
