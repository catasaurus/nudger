import markdown
import sentry_sdk
import uuid

from collections import OrderedDict
from django.db import models
from sentry_sdk import add_breadcrumb

# from django.contrib.auth.models import User


quizbot_db = [
    {"welcome": ("Quiz:\nWelcome to the Quizbot\n",
                 "Type 'start' to start quiz\n"
                 "Type 'end' to end quiz"),
     "answer": "start"},
    {"question": "5+3?", "answer": "8"},
    {"question": "7+11?", "answer": "18"},
    {"question": "4*4?", "answer": "16"},
]

UTTERANCE_LANG_DICT = {
    "English": 'en',
    "Spanish (Español)": 'es',
    "Chinese - Simplified (简体中文)": 'zh-hans',
    "Chinese - Traditional (繁體中文)": 'zh-hant'
}


def report_through_sentry(label, issue, data):
    # msg_template = "{issue}- data: {data}"
    # msg = msg_template.format(issue=issue, data=data)
    sentry_sdk.capture_message(issue, label)


def preprocess_utterance(utt):
    return str(utt).lower().strip()


def preprocess_intent_id(trigger_name):
    return preprocess_utterance(trigger_name)


class State(models.Model):
    # Foreign key to itself
    next_states = models.ManyToManyField(
        "self",
        through="Trigger",
        through_fields=("from_state", "to_state")
    )
    state_name = models.CharField(
        null=False,
        max_length=255,
        unique=True,
        help_text="The title the system uses to recognize the state"
    )
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    update_context = models.JSONField(
        null=True,
        help_text="May hold information such as gender, location, or history of previous exchanges"
    )
    level = models.IntegerField(
        null=True,
        help_text="An optional field that tells the depth of the action"
    )

    def add_language_setting_to_context(self, data):
        """ Adds lang (language setting) when called from a state

            Only supposed to be used after the initial_state loads

        >>> data = {
        ...     'state_name': 'select-language',
        ...     'user_text': 'English',
        ...     'context': {'lang':'en'}
        ... }
        >>> output = self.add_language_setting_to_context(data)
        >>> output['context']['lang']
        'en'

        >>> data = {
        ...     'state_name': 'select-language',
        ...     'user_text': 'Spanish (Español)',
        ...     'context': {'lang':'en'}
        ... }
        >>> output = self.add_language_setting_to_context(data)
        >>> output['context']['lang']
        'es'
        """
        return UTTERANCE_LANG_DICT.get(
            preprocess_utterance(data.get('user_text')), 'en')

    def create_initial_user_type_dict(self, triggers):
        """ Builds a dictionary of user_type roles from triggers in the target state """
        user_type_dict_English = {}
        user_type_dict_English_keys = []
        for trigger in triggers:
            if trigger.lang == 'en':
                user_type_dict_English[trigger.intent_text] = [trigger.intent_text]
                user_type_dict_English_keys.append(trigger.intent_text)
        return user_type_dict_English, user_type_dict_English_keys

    def update_user_type_dict(self, triggers, user_type_dict_English, user_type_dict_English_keys, lang_list):
        """ Takes an dict of user_types in English and adds corresponding entries for other languages """
        for lang in lang_list:
            tr_sublist = []
            for trigger in triggers:
                if trigger.lang == lang:
                    tr_sublist.append(trigger)
            for index in range(3):
                user_type_dict_English[user_type_dict_English_keys[index]].append(tr_sublist[index].intent_text)
        return user_type_dict_English

    def generate_user_type_dict(self):
        """ Builds a dictionary of user_types for the 'is-this-your-first-yes' state

        Used to set context dictionary's user_type
        """
        state = State.objects.filter(state_name='is-this-your-first-yes')[0]
        triggers = Trigger.objects.filter(from_state=state)

        user_type_dict_English, user_type_dict_English_keys = self.create_initial_user_type_dict(self, triggers)

        lang_dict = self.send_language_dictionary(self)
        lang_list = list(lang_dict.values())

        user_type_dict = self.update_user_type_dict(
            self,
            triggers,
            user_type_dict_English,
            user_type_dict_English_keys,
            lang_list
        )
        return user_type_dict

    def add_user_type_to_context(self, data):
        """ Adds the user_type when called from a state

        >>> data = {
        ...     'state_name': 'is-this-your-first-yes',
        ...     'user_text': 'WSNYC Volunteer + Educator',
        ...     'context': {'lang':'en'}
        ... }
        >>> output = self.add_user_type_to_context(data)
        >>> output['context']['user_type']
        'WSNYC Volunteer + Educator'

        >>> data = {
        ...     'state_name': 'is-this-your-first-yes',
        ...     'user_text': 'Estudiante de WSNYC',
        ...     'context': {'lang':'es'}
        ... }
        >>> output = self.add_user_type_to_context(data)
        >>> output['context']['user_type']
        'WSNYC Student'
        """

        user_type_dict = self.generate_user_type_dict(self)

        user_type = ''
        for val in user_type_dict.values():
            if data['user_text'] in val:
                user_type = list(user_type_dict.keys())[list(user_type_dict.values()).index(val)]

        data['context']['user_type'] = user_type
        return data

    def state_serializer(self, state):
        return {'state_name': state}

    def message_serializer(self, state, lang):
        messages = Message.objects.filter(state=state, lang=lang).values_list("sequence_number", "bot_text")

        message_serializer_list = []
        for message in messages:
            message_serializer_list.append(
                OrderedDict(
                    [
                        ('sequence_number', message[0]),
                        ('bot_text', markdown.markdown(
                            message[1], extensions=['attr_list']
                        )
                        )
                    ]
                )
            )
        return message_serializer_list

    def trigger_serializer(self, state, lang):
        triggers = Trigger.objects.filter(from_state=state, lang=lang).values_list("intent_text", "to_state")
        trigger_serializer_list = []
        for trigger in triggers:
            state_result = State.objects.get(pk=trigger[1])
            trigger_serializer_list.append(
                OrderedDict(
                    [
                        ('intent_text', trigger[0]),
                        ('to_state', state_result.state_name)
                    ]
                )
            )
        return trigger_serializer_list

    @classmethod
    def get_next_state_from_database(self, data):
        """ input the current state (state_name) + message user selected (user_text)
        get_next_state_from_database returns a JSON object of the next state's data necessary to fill the frontend.

        Input:
        {state_name: 'select-language', user_text: 'English', context: {'lang':'en'}}

        Output:
        {'state':
            {'state_name': 'selected-language-welcome'},
            'messages': [
                OrderedDict([
                    ('sequence_number', 0),
                    ('bot_text', 'I am part of the We Speak NYC team, and I am here to help you navigate our resources and services.')
                    ]),
                OrderedDict([
                    ('sequence_number', 1),
                    ('bot_text', 'I have just a few questions before we get started... Select "Ready" to get started.')
                    ])],
            'triggers': [
                OrderedDict([
                    ('intent_text', 'Ready'),
                    ('to_state', 'is-this-your-first')])],
                    context: {'lang':'en'}}

        >>> data = {
        ...     'state_name': 'select-language',
        ...     'user_text': 'English',
        ...     'context': {'lang':'en'}
        ... }
        >>> output = self.get_next_state_from_database(data)
        >>> output['state']['state_name'] == 'selected-language-welcome'
        True

        >>> data = {"state_name": "", "user_text": ""}
        >>> State.get_next_state_from_database(data)
        {"state_name": "select-language", "user_text": ""}
        """

        add_breadcrumb(
            category='Database Call Function',
            message='get_next_state_from_database',
            level='info'
        )
        self.handle_missing_arguments(data)

        # Test if the state exists in the database
        try:
            initial_state = State.objects.get(state_name=data['state_name'])
        except State.DoesNotExist as err:
            sentry_sdk.capture_exception(err)
            data['error_message'] = f'No matching state'
            return data

        # Handle the default starting state - 'select-language'
        if data['state_name'] == 'select-language' and data['user_text'] == '':
            state_serializer = self.state_serializer(self, initial_state.state_name)
            message_serializer = self.message_serializer(self, initial_state, data['context']['lang'])
            trigger_serializer = self.trigger_serializer(self, initial_state, data['context']['lang'])
            return {
                "state": state_serializer,
                "messages": message_serializer,
                "triggers": trigger_serializer,
                "context": {'lang': 'en'}
            }

        # Get available states for the initial_state
        # Allow matching of any response in any language
        triggers = Trigger.objects.filter(from_state=initial_state)
        # TODO-R: convert this queryset to an `intent_state_dict = `
        #       `T = dict(zip(*qs.values_list(['intent_text', 'to_state'])))`
        #       then use intent_state_dict in place of the for loop below

        # Test if the user_text is an option in the state
        # Having a user_text and an initial_state that's not 'select-language' should go to next_state
        if data['user_text'] == '':
            # Not select-language and empty user_text should fail
            report_through_sentry('info', 'Missing required response', data)
            content = {"error_message": "User message is required", "data": data}
            return content

        for trigger in triggers:
            if preprocess_intent_id(trigger.intent_text) == preprocess_utterance(data['user_text']):
                next_state = trigger.to_state
                break
        # if 'for' block doesn't reach break keywork following code will execute
        else:
            report_through_sentry('info', 'Unacceptable response', data)
            content = {'error_message': 'Response is not an option', 'data': data}
            return content

        # Test for and execute functions associated with a state
        state_test = State.objects.filter(state_name=data['state_name'])
        trig = Trigger.objects.filter(from_state=state_test[0], intent_text=data['user_text'])

        try:
            if 'context_functions' in trig[0].update_kwargs.keys():
                for func_name in trig[0].update_kwargs['context_functions']:
                    output = eval('State.' + func_name)(self, data)
                    data['context'] = output['context']
        except AttributeError:
            pass

        # Build the response with the data for the next state
        state_serializer = self.state_serializer(self, next_state.state_name)
        message_serializer = self.message_serializer(self, next_state, data['context']['lang'])
        trigger_serializer = self.trigger_serializer(self, next_state, data['context']['lang'])
        return {
            "state": state_serializer,
            "messages": message_serializer,
            "triggers": trigger_serializer,
            "context": data['context']
        }


    def handle_missing_arguments(self, data):
        # Set the state_name if value missing
        if not data.get('state_name'):
            report_through_sentry('info', 'Missing state_name', data)

            data['state_name'] = 'select-language'
            data['user_text'] = data.get("user_text", '')

        # Set the user_text if value missing
        data['user_text'] = data.get('user_text') or ''

        # Set the context if value missing
        try:
            if data['context'] == '' or data['context'].get('lang') == '':
                data['context'] = {'lang': 'en'}
        except (KeyError, AttributeError):
            data['context'] = {'lang': 'en'}


LANGUAGE_CHOICES = [
    ('en', 'en'),
    ('es', 'es'),
    ('zh-hans', 'zh-hans'),
    ('zh-hant', 'zh-hant')
]


class Message(models.Model):
    """ A table for messages the bot can send.
    Each row contains one message written in one language.
    """
    sequence_number = models.IntegerField(blank=False, null=False)
    state = models.ForeignKey(
        State,
        null=False,
        blank=False,
        on_delete=models.CASCADE
    )
    bot_text = models.TextField(
        null=False,
        help_text="The message of the state written in English"
    )
    lang = models.CharField(choices=LANGUAGE_CHOICES, default='en', max_length=100)


class Trigger(models.Model):
    """ A table of supported user inputs that connect two states.

    Users can enter the intent_text by button click or text input.

    Each row is one trigger option written in one language.
    """
    # Foreign Keys
    from_state = models.ForeignKey(
        State,
        related_name="rel_from_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    to_state = models.ForeignKey(
        State,
        related_name="rel_to_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    intent_text = models.CharField(
        null=False,
        max_length=255,
        help_text="The trigger message"
    )
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    lang = models.CharField(
        choices=LANGUAGE_CHOICES,
        default='en',
        max_length=100
    )


MESSAGE_DIRECTION = [
    ('inbound', 'inbound'),
    ('outbound', 'outbound'),
]

SENDER_TYPE = [
    ('user', 'user'),
    ('chatbot', 'chatbot'),
    ('human agent', 'human agent'),
]


class InteractionData(models.Model):
    """ A table that captures data each time a message is sent """
    message_id = models.UUIDField(
        default=uuid.uuid4,
        unique=True,
        help_text="A unique id assigned to each message that is sent out"
    )
    message_direction = models.CharField(
        choices=MESSAGE_DIRECTION,
        default='en',
        max_length=100
    )
    message_text = models.TextField(
        null=False,
        help_text="The message of the state"
    )
    send_time = models.DateTimeField(
        auto_now_add=True,
        help_text="The time that the message was sent (as opposed to scheduled)"
    )
    state = models.ForeignKey(
        State,
        null=True,
        max_length=255,
        on_delete=models.SET_NULL,
        help_text="The state of the chatbot when a message is sent"
    )
    sender_type = models.CharField(
        choices=SENDER_TYPE,
        max_length=20
    )
    user_id = models.CharField(
        null=True,
        max_length=255,
        help_text="A id for an anonymous user for one session"
    )
    bot_id = models.CharField(
        default="Poly Chatbot",
        max_length=255,
        help_text="The name or id of the chatbot being interacted with"
    )
    channel = models.CharField(
        default="WSNYC Website",
        max_length=128,
        help_text="The platform the user interacted with, such as WSNYC website"
    )

    """
    TODO Questions:
    * Should I incorporate status messages (like errors into this)?
    * For the state, should I return the state object or just the name
    """


# class User(models.Model):
#     first_name = models.CharField(null=False, max_length=128)
#     last_name = models.CharField(null=False, max_length=128)
#     email = models.EmailField(null=False, max_length=128, default='')
#     mobile_number = models.CharField(max_length=32)
#     birth_date = models.DateField(default='', help_text="Birthday in YYYY-MM-DD")
#     location = models.CharField(null=True, max_length=128)
#     role = models.CharField(
#         null=False,
#         max_length=128,
#         help_text="Set up as 'WSYNYC Student', 'WSNYC Volunteer + Educator', or 'General Website Visitor'"
#     )
#     selected_language = models.CharField(null=True, max_length=32, help_text="The language preference the user sets")


# class Language(models.Model):
#     language_code = models.CharField(null=False, unique=True, max_length=32, help_text="Abbreviation of the language")
#     language_name = models.CharField(null=False, max_length=128, help_text="Full name of the language")
