[![CICD Pipeline](https://gitlab.com/tangibleai/nudger/badges/main/pipeline.svg)](https://gitlab.com/tangibleai/nudger/badges/main/pipeline.svg)



# nudger
Chatbot to improve the educational experience for students.


## Installation

Clone the repository:

```bash
git clone git@gitlab.com:tangibleai/nudger.git
cd nudger
```

Create a Python virtual environment with `virtualenv` (or `conda`), and install dependencies there:

### `virtualenv` (preferred approach)

```bash
pip install --upgrade pip virtualenv poetry wheel
python -m virtualenv --python=3.9 .venv
source .venv/bin/activate
```

### Anaconda (conda)

```bash
conda create -y -n nudgerenv 'python=3.9.16'
conda activate nudgerenv
# conda env update -n nudgerenv -f environment.yml
```

## Build (install)

Once you have a clean virtual environment activated you can install all the dependencies and migrate your databases.
See `scripts/build.sh` if you want to run all the commands manually.
If `source scripts/build.sh` doesn't work on your OS, you will need to modify the `build.sh` script.
Here's the main thing you need to install the dependencies and add the `nudger` package to the `PYTHON_PATH` in your virtual environment.

#### *`scripts/build.sh`* 
```bash
pip install -e .
```

## Deploy (or run)

### Local development server

#### *`scripts/start-django-dev.sh`*
```bash
python manage.py runserver
```

### Production server on render

Three services/servers on render.com:

1. nudger-main: django app (daphne async web server) `daphne -b 0.0.0.0 nudger.asgi:application`
2. nudger-celery-main render server: start-celery.sh (with a copy of django app to be able to run django tasks)
3. nudger-redis: RAM database for django to communicate tasks to celery server
