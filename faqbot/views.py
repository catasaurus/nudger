from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.views.generic import ListView, CreateView
from rest_framework.decorators import api_view
from rest_framework.response import Response

from faqbot.models import Faq, MathQuestion, StudentQuestion


class FaqListView(LoginRequiredMixin, ListView):
    """Returns questions as list"""
    model = Faq
    template_name = 'faqbot/faq_list.html'
    context_object_name = 'faqs'
    ordering = ['-created_on']  # '-' to descending
    paginate_by = 10

    def get_queryset(self):
        """Overriding query method"""
        return Faq.objects.filter(user_id=self.request.user.id).order_by('-created_on')


class FaqCreateView(LoginRequiredMixin, CreateView):
    """Records questions to DB"""
    model = Faq
    fields = ['question']
    template_name = "faqbot/faq_create.html"
    DEFAULT_QUESTION = "Who are you?"
    DEFAULT_ANSWER = "I'm Qary!"

    def form_valid(self, form):
        question = form.cleaned_data.get("question")
        answer = self.DEFAULT_ANSWER if question == self.DEFAULT_QUESTION else "I don't know that!"
        form.instance.user_id = self.request.user
        form.instance.answer = answer
        form.save()
        return JsonResponse({"question": question, "answer": answer})


@api_view(["POST"])
def math_question(request):
    """Returns the answer of the question if exists in the database
    else returns a static answer
    """
    question = request.POST.get("question")
    try:
        question_object = MathQuestion.objects.get(question=question)
    except ObjectDoesNotExist:
        return Response({"answer": "Sorry, I am not sure about that!"})
    else:
        return Response({"answer": question_object.answer})


@api_view(["POST"])
def student_question(request):
    """Returns the answer of the question if exists in the database
    else returns a static answer
    """
    question = request.POST.get("question")
    try:
        question_object = StudentQuestion.objects.get(question=question)
    except ObjectDoesNotExist:
        return Response({"answer": "Sorry, I am not sure about that!"})
    else:
        return Response({"answer": question_object.answer})
