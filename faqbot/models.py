from django.contrib.auth.models import User
from django.core.validators import MaxLengthValidator
from django.db import models


# Create your models here.
class Faq(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.TextField(default="Who are you?", validators=[MaxLengthValidator(255)])
    answer = models.TextField(default="")

    def __str__(self):
        return f'{self.question} {self.user_id}'


class MathQuestion(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    question = models.TextField(default="", validators=[MaxLengthValidator(255)])
    answer = models.TextField(default="")

    def __str__(self):
        return f'{self.question} {self.answer}'


class StudentQuestion(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    question = models.TextField(default="", validators=[MaxLengthValidator(255)])
    answer = models.TextField(default="")

    def __str__(self):
        return f'{self.question} {self.answer}'
