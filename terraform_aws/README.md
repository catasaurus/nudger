# Nudger Terraform

ensure you have terraform installed
```bash
terraform -version
```

[install instuctions](https://learn.hashicorp.com/tutorials/terraform/install-cli)

for Ubuntu install

```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
```



## setup credentials with environment variables

Terraform and aws look for keys in different ways,
you can use the same keys for both or have different keys for the backend.
I used the same keys with both formats.

```bash
# terrform:
export TF_VAR_AWS_ACCESS_KEY="key1_CHANGE_ME"
export TF_VAR_AWS_SECRET_KEY="key2_CHANGE_ME"

# aws:
export AWS_ACCESS_KEY="key1_CHANGE_ME"
export AWS_SECRET_KEY="key2_CHANGE_ME"
```

##  setup ssh

```bash
chmod +x scripts/setup_ssh.sh
project_name='nudger' scripts/setup_ssh.sh
```



##  run Terraform

from the terraform directory

```bash
pwd
# .../nudger/terraform
terraform init
terraform plan
```

> *Error: Error acquiring the state lock*

You will get this error on the initial launch because
we are setting up a remote backend -to share with our fellow devs.

So for the initial provision we will use the `-lock=false` argument
on the `plan` and `apply` because we are establishing the lock file in a DynamoDB.

```bash
pwd
# .../nudger/terraform
terraform plan -lock=false
```

if the plan looks good run `terraform apply`
```bash
pwd
# .../nudger/terraform
terraform apply -lock=false
```

To make further changes `-lock=false` is not needed as the backend is now created.
The next use of the `-lock=false` argument should be when destroying *ALL* the resources

## TODO
* [ ] finish instructions to run terraform script
