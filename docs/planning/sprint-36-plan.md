# Sprint 35 (Nov 25 - Dec 02)

## High priority

* [x] R1-0.5: Create a document called `deploy.md` for MOIA team
* [x] R0.5-0.2: Download the source code of the wespeaknyc.cityofnewyork.us website
* [x] R2-2: Examine the `index.html` and fix the URL/file paths to get it working locally
* [x] R1.5-2: Deploy the downloaded wespeaknyc.cityofnewyork.us website
* [x] R0.2-0.2: Embed the widget into the downloaded web page
* [x] R0.1-0.1: Create a new file called `deploy-bugs.md`
* [x] R0.1-0.2: Test the widget by opening the web page and having two interactions 
* [x] R2-2: Test for one or two hours in order to search for new bugs
* [x] R1-0.2: Add one more interaction to the integration test
* [x] R1-0.8: Add three more intractions to the integration test
* [x] R0.1-0.1: Add delay before every interaction
* [x] R0.2-0.2: Try to run integration tests in two terminals
* [x] R0.5-0.5: Launch the integration tests in five or seven terminals

## Medium priority

* [x] R0.2-0.2: Generate a new bundle files for `quizbot`
* [x] R0.1-0.1: Rename the generated bundles to `mathbot.css` and `mathbot.js`
* [x] HR1-0.5: Review Cetin's merge request
* [x] HR0.2-0.2: Merge Cetin's branch into `main`
* [x] R1-0.5: Help Cetin embed the newly updated bundles into the `quizbot` app

## Low priority

* [] R0.5: Find wespeak... web page in archive.org
* [] R0.5: Download the wespea... web page from archive.org
* [] R1: Reply to the invalid input by "Sorry, I don't understand you" from the backend
* [] R1: Clean up the `main` branch of `moia-poly-chatbot` repository from unused files
* [] R0.2: Merge the `secure` branch into `main`


### Done Sprint 35 (Nov 18 - Nov 25)

* [x] R?: Add a new parameter named **ws_url** to the `settings.json` file
* [x] R?: Modify the `bundle.js` file to read the **ws_url** parameter from `settings.json`
* [x] R?: Make the `settings.json` file global to be modified by external apps
* [x] R?: Ensure the `settings.json` is working globally by changing the actual parameters
* [x] R?: Fix the bug **Cross-Origin Request Blocked** 