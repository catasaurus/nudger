# Sprint 2 (Aug 12 - Aug 19)

* [x] R0.5-0.2: Merge the new accomplished changes of the last sprint into `main`
* [x] R0.2-0.2: Make descriptive names for the variables x, y, and z
* [x] R0.1-0.1: Create a new file called `settings.json`
* [x] R0.1-0.1: Add the first configurables to the `settings.json` file
* [x] R0.2-0.2: Install and integrate `@rollup/plugin-json` plugin in the app
> The tasks listed below can be divided into additional tasks
* [x] R3-2: Refactor/restucture the source code to make it easy to configure
* [x] R2-1.5: Create a configurable that turn on/off the greeting component
* [x] R2-1: Create a configurable that allow/prevent empty messages to be sent

* [x] G4-1.5: reduce Daphne startup delay (figure out what can be reduced) - removed some requirements installation and upgraded to $7/mo plan
* [x] G6-8.5: Extract a value from user input (user_type)
  - extract user_type from states (i.e., is-this-your-first-no, is-this-your-first-yes)
  - save the entire message extracted value to the user context dictionary
  - ensure the saved value is preserved as the conversation is written
  - build a way to trigger this save function into yaml
  - make it more generalizable - not just for user type but also lang setting/context (calls `add_user_type_to_context` `add_language_to_context`?)
* [x] auto-formatting working for sublime and anaconda plugin
* [x] G8-10: Change message queue to use Celery for queueing/scheduling
  - the bot should send each part of the state individually in order (user-text, bot-text, triggers)
* [x] G4-1: Handle creating a unique room name for each user
  - make a view that automatically redirects to a hardcoded room
  - research hashing options (birthday problem)
  - build a function that makes a unique value for room name
  - alphanumeric string (7 characters) (10+26+26)^7
* [x] G2-2.5: added a new log interaction function with new table models to record full interaction data (Maria's spreadsheet of state bot name inbound outbound)

* [ ] G2: get main branch working on render and deploying to qary.ai (digital ocean?) staging.qary.ai is on render
* [ ] G1: git tag -a 0.0.1 -m 'initial internal release' (any time)
* [ ] G1: git tag -a 0.0.2 -m 'handoff to rochdi internal release' (at end of sprint)
* [ ] G2: delete old branches
* [ ] G2: delete old interaction log or consolidate two log tables and functions
* [ ] G2: Create conversation management models_qary.py 
  - copy over models.py into models_qary.py
  - consolidate NextStateReply function into models_qary.py

## Done: Sprint 1 (Aug 05 - Aug 12)

* [x] R0.2-R0.1: Clone the [svelte-tawk](https://github.com/hellpirat/svelte-tawk-to-chat-widget) repository
* [x] R1-1: Adapt what is useful from the [tawk tutorial](https://raw.githubusercontent.com/codebubb/tawk-tutorial/main/src/Tawk.js) repo 
* [x] H?: Create a new repo named `faceofqary` and make it public
* [x] R0.1-0.1: Clone the `faceofqary` repository
* [x] R0.2-0.1: Edit the README file by adding the project plan below
* [x] R0.2-0.1: Create an empty Svelte project called **client**
* [x] R0.2-0.1: Add a `.gitignore` file to ignore node_modules and build files
* [x] R0.2-0.1: Push the empty svelte project to the `faceofqary` repository
* [x] R3-R2.5: Build a chat widget component using Svelte
> 
* [x] R1-1: Create a chat-window component that contains an empty text box and chat header
* [x] R1-1: Add a **send** that sends messages
* [x] R0.5-0.5: Up-scroll previous messages in the same window
* [x] R0.5-0.5: Make the **chat conversation** button initialize the chat (welcome message)
* [x] R0.5-1: Make the chat bullet echo what the user says in the chat 
* [x] R0.5-0.5: Delay the welcome message
* [x] R0.5-0.5: Edit the **start conversation** button when the chat starts
>
* [x] R0.5-0.2: Create an empty django project in the same repository
* [x] R1-0.5: Structure the repository using the monorepo multi-server approach
* [x] R1-0.5: Create two web services called **faceofqary-front** and **faceofqary-back**
* [x] R1-1: Deploy the application on Render one step at a time

